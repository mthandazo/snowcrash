AUTHOR: Mthandazo Ndhlovu
Login: mndhlovu
Level: 05
Tools: N/a

Upon authenticating as level05, I recieved an email notification. Navigating to /var/mail to check the email I just received, I noticed a file labelled level05. Checking the permissions of the file, I noticed that it was owned by root and level05 only had executable rights. 

By analysing the contents of the file, I discovered that it was a cronjob script.The contents of the script where as follows:

*/2 * * * * su -c "sh /usr/sbin/openarenaserver" - flag05

The cronjob seems to execute as flag05 /usr/sbin/openarenaserver every minute.

Observing the script which the cronjob executes, i discovered that it simply iterates the contents of /opt/openarenaserver, sets the ulimit to 5, executes the command located at the cursor of the iterator and removes it from the directory. 

As a PoC, I will place a basic script inside /opt/openarenaserver and make sure it directs its output to a file in /tmp.

PoC:
echo "echo 'About to be Pawned by Mndhlovu' > /tmp/results.txt" > /opt/openarenaserver/evilscript

After waiting for a minute the cronjob executed and a file results.txt was created in the tmp folder.

My final exploit was as follows:

echo "getflag > /tmp/results.txt" > evilscript

After 2 minutes, I check the contents of /tmp/results.txt and Bingo i found the level06 password.

username: level06
password: viuaaale9huek52boumoomioc

PAWNED !!
