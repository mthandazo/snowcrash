AUTHOR: MTHANDAZO NDHLOVU
LOGIN: mndhlovu
LEVEL: 00
Tools: caesar-cracker.py

Upon authenticating as username level01, I did some basic but crucial recon.

Using find to enumerate all files owned by flag00:

find / -user flag00 -group flag00
I noticed two interesting files:
1. /usr/sbin/john
2. /rofs/usr/sbin/john

Analysis of the content inside the files I found the following text:
cdiiddwpgswtgt

I tried switching to flag00's account using the above text as my password but I got an auth error.

I suspect the text is a hash, although I'm not quite sure the hash-type yet.

Analysing the frequency of each letter in the hash:
c ==> 1
d ==> 3
i ==> 2
w ==> 2
p ==> 1
g ==> 2
s ==> 1
t ==> 2

Using the relationship between the letter frequency, letter distribution and characterists of different ciphers i was able to deduce the possibility of the hash being a product of the caeser-cipher.

To encrypt text using the caeser-cipher you provide a key which determines how each letter will be shifted. This key is also required in order the reverse the encryption process and unveil the actual text. Since I don't have a key, I will use bruteforcing means and try to find all possible texts.

Using my caesar-cracker.py which is also available in the same folder as this walkthrough, I got the following text results for different keys:

Key | decrypted text
0	| cdiiddwpgswtgt
1	| bchhccv0frvsfs
2	| abggbbunequrer
3	| xaffaatmdptqdq
4	| yxeexxslcospcp
5	| zyddyyrkbnrobo
6	| wzcczzqjamqnan
7	| vwbbwwpixlpmxm
8	| uvaavvohykolyl
9	| tuxxuungzjnkzk
10	| styyttmfwimjwj
11	| rszzsslevhlivi
12	| qrwwrrkdugkhuh
13	| pqvvqqjctfjgtg
14	| opuuppibseifsf
15	| nottoohardhere
16	| mnssnngxqcgdqd
17	| lmrrmmfypbfcpc
18	| klqqllezoaebob
19	| jkppkkdwnxdana
20	| ijoojjcvmycxmx
21	| hinniibulzbyly
22	| ghmmhhatkwazkz
23	| fgllggxsjvwxjw
24	| efkkffyriuyviv
25	| dejjeezqhtzuhu

Through trial and error i managed to authenticate to flag00's account by using nottoohardhere as my password.

Launching getflag I got the password for level01.

username: level01
password: x24ti5gi3x0ol2eh4esiuxias
