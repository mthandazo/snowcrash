"""
AUTHOR: Mthandazo Ndhlovu
Login: mndhlovu
Prog name: caesar-cracker.py
Description: basic caesar hash cracker 
"""

encypted_text = "cdiiddwpgswtgt" #encrypted text
all_letters = "abcdefghijklmnopqrstuvwxyz"

for key in range(len(all_letters)):
    results = ""
    for letter in encrypted_text:
        if letter in all_letters:
            num = all_letters.find(letter)
            num = num - key
            if num < 0:
                num = num + len(all_letters)
            results = results + all_letters[num]
        else:
            results = results + letter
    print(f"For key: {str(key)} the password is: {results}")
